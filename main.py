import config
from aiotg import Bot, Chat
import requests
import asyncio
from datetime import datetime
from lxml import etree
from xml.etree import ElementTree
import re

bot = Bot(api_token=config.token)

@bot.command(r"^\/(sa)\s((\b\d{1}\b)|(\S{4})).*")  # only actual
async def sa_reply(chat: Chat, match):
    print("SA matched")
    await say(chat, get_api_response(match.group(0), ['SA']))
    log(str(datetime.now()) + " " + chat.sender["first_name"] + " " + match.group(0) + "\n")


@bot.command(r"^\/(fc|ft)\s((\b\d{1}\b)|(\S{4})).*")  # Only forecast
async def ft_reply(chat: Chat, match):
    await say(chat, get_api_response(match.group(0), ['FT']))
    log(str(datetime.now()) + " " + chat.sender["first_name"] + " " + match.group(0) + "\n")


@bot.command(r"^\/(sa|ft|fc)\s+(sa|ft|fc)\s((\b\d{1}\b)|(\S{4})).*")  # Both actual and forecast
async def both_reply(chat: Chat, match):
    await say(chat, get_api_response(match.group(0), ['SA', 'FT']))
    log(str(datetime.now()) + " " + chat.sender["first_name"] + " " + match.group(0) + "\n")

@bot.command(r"^\/help")
def help_reply(chat: Chat, match):
    reply = 'Type "/SA" or "/FC" or "/FT" or ""/SA FT" followed by ICAO codes, separated by space' + \
            '\nAdd number of hours from 1 to 9 to check history (only METAR)' + \
            '\nExample: "/SA ULMM UWLW 2" or "/FC EHAM KATL KBHM" or "/SA FT ULMM ULLI UUEE"'
    chat.send_text(reply)
    log(str(datetime.now()) + " " + chat.sender["first_name"] + " " + match.group(0) + "\n")

@bot.command(r"^\/about")
def help_reply(chat: Chat, match):
    reply = 'Service is based on adds server data. Developed by Pavel Belov, Volga-Dnepr airlines (c) 2018'
    chat.send_text(reply)
    log(str(datetime.now()) + " " + chat.sender["first_name"] + " " + match.group(0) + "\n")

@bot.default
def deflt(chat, message):
    chat.send_text('Invalid input. Type /help for syntax.')
    log(str(datetime.now()) + " " + chat.sender["first_name"] + " help " + "\n")

async def say(chat, reply_function):
    async for message in reply_function:
        chat.send_text(message)

def log(msg):
    with open("log.txt", 'a') as log:
        log.write(msg)
        log.close()

async def get_api_response(full_request_text, types_list):
    sorted_list = ['empty']
    for index in types_list:
        print(index)

        #Look for ICAO codes groups
        icao_list = re.findall(r'\b\S{4}\b', full_request_text)[:4]
        stations_string = '%20'.join(icao_list)

        #Look for one digit in request'''
        one_digit_found = re.search(r'\b\d{1}\b', full_request_text)
        number = int(one_digit_found.group(0)) if one_digit_found != None else 1
        period = number if number in range(1, 9) else 1

        #Only recent parameter
        only_recent = ''
        if (index == 'SA') & (period == 1):
            only_recent = '&mostRecentForEachStation=true'
        elif index == 'FT':
            period = 6
            only_recent = '&mostRecentForEachStation=postfilter'

        #Create api request
        url_request = config.template.format(type=config.types[index], stations=stations_string, hours=period, recent=only_recent)

        #Get response
        try:
            response = requests.get(url_request)
            tree = ElementTree.fromstring(response.content)
            if tree.find(".//data").get('num_results') != "0":
                raw = tree.findall(".//raw_text")
                result = []
                for item in raw:
                    result.append(item.text)
                    sorted_list = '\n\n'.join(sorted(result, reverse=True))
            else:
                sorted_list = ['No reply from server']
        except requests.exceptions.RequestException as error:
            errmsg = 'Server error: %(err), try again' % {'err': error}
            sorted_list = errmsg

        print(sorted_list)
        yield sorted_list

if __name__ == '__main__':
    bot.run()
    #loop = asyncio.get_event_loop()
    #loop.run_until_complete(bot.loop())
